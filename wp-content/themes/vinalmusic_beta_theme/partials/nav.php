<?php 
	$link_home = get_site_url( $blog_id = null, $path = '', $scheme = null );
	$themedir = get_template_directory_uri();
 ?>

<div class="nav">
	<div class="nav-menu revealer">
		<i class="fa fa-navicon nav-menu-navicon"></i>
		<a class="nav-menu-logo" href="<?php echo $link_home; ?>"><img src="<?php echo $themedir . '/library/img/whitelogo.png'; ?>" alt=""></a>
	</div>
</div>
<?php 
	//	Sidebar (mobile - desktop)
 ?>
<div class="sidebar">
	<div class="sidebar-menu">

		<div class="sidebar-menu-links">

			<p class="button"><a href="https://vinalband.bandcamp.com/" class="button-text">Buy E.P.</a></p>
			<p class="button sidebar-menu-links-contact"><a href="#" class="button-text">Contact</a></p>

		</div>


		<div class="sidebar-menu-socials">
			<?php 
				if( have_rows('social_media', 'options') ) : while ( have_rows('social_media', 'options') ) : the_row();
					$social = get_sub_field('link');
					$icon = get_sub_field('icon');
			 ?>
 				<a class="sidebar-menu-socials-social" href="<?php echo $social['link']; ?>">
 					<i class="sidebar-menu-socials-social-icon fa <?php echo $icon; ?>"></i>
					<span class="sidebar-menu-socials-social-title"><?php echo $social['title']; ?></span>
 				</a>
			<?php endwhile; endif; ?>
		</div>
	</div>
</div>