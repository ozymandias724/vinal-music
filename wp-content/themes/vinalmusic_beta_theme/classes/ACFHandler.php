<?php 
/*
	Registers Advanced Custom Fields
	https://www.advancedcustomfields.com/resources/register-fields-via-php/
	https://www.advancedcustomfields.com/resources/acf_add_options_page/
*/
/*
	Add Options Pages
*/
	if( function_exists('acf_add_options_page') ) {
		// General Settings Options Page
		$option_page = acf_add_options_page(array(
			'page_title' 	=> 'Site Settings',
			'menu_title' 	=> 'Site Settings',
			'menu_slug' 	=> 'custom-settings',
			'capability' 	=> 'edit_posts',
			'redirect' 	=> false,
			'position' => 4.11,
		));
		// HomePage Settings Options SubPage
		$option_page_homepage = acf_add_options_page(array(
				'page_title' 	=> 'Home Page',
				'menu_title' 	=> '',
				'menu_slug' 	=> 'homepage',
				'capability' 	=> 'edit_posts',
				'redirect' 	=> false,
				'parent_slug' => 'custom-settings',
			));
	}
/*
	Create Field Groups
*/
	if( function_exists('acf_add_local_field_group') ) :
		// General Settings Wrapper
		acf_add_local_field_group(array(
			'key' => 'group_6kl64no2i',
			'title' => 'General Site Settings',
			'fields' => array (),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'custom-settings',
					),
				),
			),
		));
		// Homepage Settings Wrapper
		acf_add_local_field_group(array(
			'key' => 'group_5ds8g0a9s81g',
			'title' => 'Home Page Settings',
			'fields' => array (),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'homepage',
					),
				),
			),
		));	
	/*
		Create Fields; Add them to Groups
		(CHECK PARENT FIELDS)
	*/
		
		// Add SocialMedia Repeater
		acf_add_local_field(array(
			'key' => 'field_zo235nog8gn',
			'label' => 'Social Media',
			'name' => 'social_media',
			'type' => 'repeater',
			'layout' => 'table',
			'parent' => 'group_6kl64no2i' // General Options
		));
			// SocialMedia Repeater Fields
			acf_add_local_field(array(
				'key' => 'field_235ln436oindki',
				'label' => 'Icon',
				'name' => 'icon',
				'type' => 'font-awesome',
				'parent' => 'field_zo235nog8gn',
				'save_format' => 'class',
			));
			acf_add_local_field(array(
				'key' => 'field_j3843b4g98aksj',
				'label' => 'Link',
				'name' => 'link',
				'type' => 'link',
				// 'return_format' => 'url',
				'parent' => 'field_zo235nog8gn',
			));
		// BandMember Repeater
		acf_add_local_field(array(
			'key' => 'field_w6e51gw2wer0g',
			'label' => 'Band Members ',
			'name' => 'band_members',
			'type' => 'repeater',
			'parent' => 'group_5ds8g0a9s81g' // HomePage Options
		));
			// BandMember Repeater Fields
			acf_add_local_field(array(
				'key' => 'field_235oingpoi',
				'label' => 'Name',
				'name' => 'name',
				'type' => 'text',
				'parent' => 'field_w6e51gw2wer0g'
			));
			acf_add_local_field(array(
				'key' => 'field_2agv24gi',
				'label' => 'Instrument',
				'name' => 'instrument',
				'type' => 'text',
				'parent' => 'field_w6e51gw2wer0g'
			));
			acf_add_local_field(array(
				'key' => 'field_23gg65s1gi',
				'label' => 'Image',
				'name' => 'image',
				'type' => 'image',
				'parent' => 'field_w6e51gw2wer0g',
				'return_format' => 'url',
			));
		// YouTube oEmbed / Featured Video Repeater
		acf_add_local_field(array(
			'key' => 'field_k2lon26onigw23',
			'label' => 'Featured Videos',
			'name' => 'featuredvideos',
			'type' => 'repeater',
			'parent' => 'group_5ds8g0a9s81g'
		));
			// YouTube Repeater Fields
			acf_add_local_field(array(
				'key' => 'field_onag8aognkd',
				'label' => 'YouTube Link',
				'name' => 'youtube',
				'type' => 'oembed',
				'width' => '',
				'height' => '',
				'wrapper' => array(
					'width' => '100',
				),
				'parent' => 'field_k2lon26onigw23'
			));

	endif;
 ?>