// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license
(function() {
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }
    if (!window.requestAnimationFrame)
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());
var APP = {
	// ValuePassed, Min1, Max1, Min2, Max2 (returns ValuePassed = ratio)
	_map : function(n,i,o,r,t){return i>o?i>n?(n-i)*(t-r)/(o-i)+r:r:o>i?o>n?(n-i)*(t-r)/(o-i)+r:t:void 0;},
	isElInView: function(el){
		if( typeof jQuery === 'function' && el instanceof jQuery){
			el = el[0];
		}
		var rect = el.getBoundingClientRect();
		return (
			rect.top >= 0 &&
			rect.left >= 0 &&
			rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
			rect.right <= (window.innerWidth || document.documentElement.clientWidth)
		);
	},
	isElHalfInView: function(el){
		if( typeof jQuery === 'function' && el instanceof jQuery){
			el = el[0];
		}
		var rect = el.getBoundingClientRect();
		var slice = el.clientHeight / 2;
		return (
			rect.top >= ( 0 - slice ) &&
			rect.left >= 0 &&
			rect.bottom <= (window.innerHeight + slice || document.documentElement.clientHeight + slice) &&
			rect.right <= (window.innerWidth || document.documentElement.clientWidth)
		);
	},
	_isHidden: function(el){	// returns true if el is display none or fixed; or if any ancestor has display none 
		return (el.offsetParent === null);
	},
};
;(function ( $, APP, window, document, undefined ) {
	$(document).ready(function(){
		FBevents = {
			pastevents: $('.events-past'),
			togglepast: $('.events-showrecent .button'),
			toggletext: $('.events-showrecent .button-text'),

			_clickHandler:function(e){
				if( FBevents.toggletext.hasClass('events-showrecent--hiderecent')){
					FBevents.pastevents.slideUp();
					FBevents.toggletext[0].innerHTML = 'Show Recent Events';
					FBevents.toggletext.removeClass('events-showrecent--hiderecent');
				}
				else {
					FBevents.pastevents.slideDown();
					FBevents.toggletext[0].innerHTML = 'Hide Recent Events';
					FBevents.toggletext.addClass('events-showrecent--hiderecent');
				}
			},
			_init: function(){
				$(FBevents.togglepast).on('click', FBevents._clickHandler);
			}
		}
		FBevents._init();

		revealer = {
			els: $('.revealer'),
			_scrollResizeLoadHandler: function(){
				for (var i = 0; i < revealer.els.length; i++) {
					( APP.isElHalfInView(revealer.els[i]) && !APP._isHidden(revealer.els[i]) )			?
						$(revealer.els[i]).addClass('revealer--revealed')		:
						$(revealer.els[i]).removeClass('revealer--revealed')			;
				}
			},
			_init: function(){
				$(window).on('scroll resize load', revealer._scrollResizeLoadHandler);
			},
		}
		revealer._init();

		contactform = {
			submit: $('.wpcf7-submit'),
			inputs: $('.wpcf7-form-control:not(".wpcf7-submit")'),
			_submission: function(e){
				if( e.detail.status == 'validation_failed' ){
					contactform.submit.addClass('cf7-submitfailed');
					contactform.submit[0].value = 'Try Sending Again?';
				}
				if( e.detail.status == 'mail_failed' ){
					contactform.submit.addClass('cf7-submitfailed');
					contactform.submit[0].value = 'Sorry, Can\'t Send';
				}
				if(e.detail.status == 'mail_sent' || e.detail.status == 'mail_sent_ok'){
					contactform.submit.addClass('cf7-submitsent');
					contactform.submit[0].value = 'SENT! Thanks!';
				}
				contactform.submit.removeClass('cf7-submitfailed');
				contactform.submit.removeClass('cf7-submitsent');
				for (var i = 0; i < contactform.inputs.length; i++) {
					$(contactform.inputs[i]).removeClass('input--filled');
				}
			},
			_inputHandler:function(e){
				// style inputs w/ text
				( $(e.target).val() )						?
					$(e.target).addClass('input--filled')	:
					$(e.target).removeClass('input--filled');
			},
			_init: function(){
				document.addEventListener('wpcf7submit', contactform._submission);
				document.addEventListener('change', contactform._inputHandler);
			}
		}
		contactform._init();

		aboutband = {
			slides: $('.desktop-bandbios-imgframe-image'),
			buttons: $('.desktop-bandbios-members-member'),
			_clickHoverHandler: function(e){
				if( e.type == 'click'){
				} else if ( e.type == 'mouseenter') {
					// HoverStyle the Button
					$(aboutband.buttons).removeClass('selected-bandmember');
					$(this).addClass('selected-bandmember');
					// Show the Slide
					$(this).addClass('chosen-bandmember');
					$i = $(this).index();
					$(aboutband.slides).removeClass('selected-bandmember-image');
					$(aboutband.slides[$i]).addClass('selected-bandmember-image');
				} else {
					// mouse left
				}
			},

			_init: function(){
				aboutband.buttons.on('mouseenter mouseleave click', aboutband._clickHoverHandler);
			}
		}
		aboutband._init();



		Sidebar = {
			mobile_navicon: $('.nav-menu-navicon'),
			overlay: $('.sidebar'),

			contact: $('.sidebar-menu-links-contact'),
			contactBanner: $('.contact-banner').offset().top,

			_scrollToContact: function(e){
				e.preventDefault();
				$(window).scrollTop(Sidebar.contactBanner);
				Sidebar._slideToggle();
			},
			_slideToggle: function(e){
				$(Sidebar.overlay).toggleClass('sidebar--open');
			},
			_resizeHandler: function(e){
				if( e.device == 'desktop' && $(Sidebar.overlay).hasClass('sidebar--open')){
					$(Sidebar.overlay).removeClass('sidebar--open');
				}
			},
			_init : function(){
				$(document).on('breakpoint', Sidebar._resizeHandler);

				Sidebar.contact.on('click', Sidebar._scrollToContact);
				Sidebar.mobile_navicon.on('click', Sidebar._slideToggle);
			},
		}
		Sidebar._init();




		parallax = {
			strength : 25,
			stronger : 40,
			imagesections : $('.parallaxbg'),
			_init : function(){
				$(window).on('resize scroll load', parallax._resizeLoadScrollHandler);
			},
			_map : function(n,i,o,r,t){return i>o?i>n?(n-i)*(t-r)/(o-i)+r:r:o>i?o>n?(n-i)*(t-r)/(o-i)+r:t:void 0;},
			_getAmount : function(el, strength){
				var windowcenter = $(window).scrollTop() + ( $(window).height() / 2 );
				var sectioncenter = $(el).offset().top + ($(el).height() / 2);
				var sectiondelta = windowcenter - sectioncenter;
				var scale = parallax._map(sectiondelta, 0, $(window).height(), 0, strength);
				return -50 + scale + '%';
			},
			_resizeLoadScrollHandler : function(){
				for(var i = 0; i < parallax.imagesections.length; i++){
					$(parallax.imagesections[i]).css('transform', 'translate3d(-50%, ' + parallax._getAmount(parallax.imagesections[i], parallax.strength) + ',0)');
				}
			}
		}
		parallax._init();

		ContactSubmission = {
			reqfields: $('.wpcf7-validates-as-required'),
			reqmarker: '<i class="fa fa-exclamation cf7-required-icon"></i>',
			submitbtn: $('.wpcf7-submit'),
			_eventHandler: function(e){
				if (e.type == 'wpcf7invalid') {
					ContactSubmission.reqfields.after(ContactSubmission.reqmarker);
					ContactSubmission.submitbtn.attr('value', 'Oops!');
				}
				if (e.type == 'wpcf7mailsent') {
					ContactSubmission.submitbtn.attr('value', 'Thanks!');
				}
			},
			_init: function(){
				document.addEventListener('wpcf7invalid', ContactSubmission._eventHandler, false );
				document.addEventListener('wpcf7mailsent', ContactSubmission._eventHandler, false );
			},
		}
		ContactSubmission._init();



		// this is a global component used to determine which breakpoint we're at
		  // to listen for the event do $(document).on('breakpoint', eventHandlerCallback);
		  // callback accepts normal amount of params that a js event listener callback would have
		  // but there's a custom property added to the event object called "name"
		  // assuming in the callback the event object variable passed in is e: console.log(e.device);
		  APP.Breakpoint = {
		   name : '',
		   direction : '',
		   prevWW : $(window).width(),
		   breakpoints : {
		    'xlarge' : 1200,
		    'large' : 992,
		    'medium' : 768,
		    'small' : 576,
		    'xsmall' : 0,
		   },
		   _init : function(){
		    $(window).on('resize load', APP.Breakpoint._resizeLoadHander);
		   },
		   _resizeLoadHander : function(e){
		    if(e.type == 'resize'){
		     if( APP.Breakpoint.prevWW > $(window).width() ){
		      APP.Breakpoint.direction = 'down';
		     }
		     else{
		      APP.Breakpoint.direction = 'up'; 
		     }
		    }
		    APP.Breakpoint.prevWW = $(window).width();

		    $.each(APP.Breakpoint.breakpoints, function(key, value){
		     var objkeys = Object.keys( APP.Breakpoint.breakpoints );
		     // if last pair in breakpoints
		     if( key == objkeys[ objkeys.length - 1 ] ){
		      if( $(window).width() < objkeys[objkeys.indexOf(key) - 1] && APP.Breakpoint.name != key ){
		       APP.Breakpoint.name = key;  
		       APP.Breakpoint._dispatchEvent();
		      }
		     }
		     // if first pair in breakpoints
		     else if ( key == objkeys[0] ){
		      if( $(window).width() > value - 1 && APP.Breakpoint.name != key ){
		       APP.Breakpoint.name = key;
		       APP.Breakpoint._dispatchEvent();
		      }
		     }
		     // if other pairs in breakpoints
		     else{
		      if( $(window).width() <= APP.Breakpoint.breakpoints[objkeys[objkeys.indexOf(key) - 1]] - 1 && $(window).width() > value - 1 && APP.Breakpoint.name != key ){
		       APP.Breakpoint.name = key; 
		       APP.Breakpoint._dispatchEvent();
		      }
		     }
		    });
		   },
		   /**
		    * [_is run conditional test on breakpoint name]
		    * @param  {string}  op   accepted operators are < > == <= >=
		    * @param  {string}  name accepted names are in breakpoints prop
		    * @return {bool}      
		    */
		   _is : function(op, name){
		    var objkeys = Object.keys( APP.Breakpoint.breakpoints ).reverse();
		    var currentIndex = objkeys.indexOf( APP.Breakpoint.name );

		    if( objkeys.indexOf( name ) !== -1 ){
		     if( op == '>' || op == '<' || op == '==' || op == '<=' || op == '>=' ){
		      switch(op){
		       case '>':
		        return currentIndex > objkeys.indexOf(name);
		        break;

		       case '<':
		        return currentIndex < objkeys.indexOf(name);
		        break;

		       case '==':
		        return currentIndex == objkeys.indexOf(name);
		        break;

		       case '<=':
		        return currentIndex <= objkeys.indexOf(name);
		        break;

		       case '>=':
		        return currentIndex >= objkeys.indexOf(name);
		        break;
		      }
		     }
		     else{
		      console.error('Invalid first param in _is. Current accepted values are >, <, ==, <= and >=');
		     }
		    }
		    else{
		     console.error('Invalid second param in _is');
		    }
		   },
		   _dispatchEvent : function(){
		    $(document).trigger($.Event('breakpoint', {device: APP.Breakpoint.name, direction : APP.Breakpoint.direction}));
		   },
		  }
		  APP.Breakpoint._init();
	});
})( jQuery, APP, window, document );