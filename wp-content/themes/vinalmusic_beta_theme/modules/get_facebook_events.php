<?php 




	// Page Identification and Access
	$pageid = '751420614907352';
	$access_token = "1779089695742062|81VRDc6lcXhRm0Gc46czB2WZVME";
	
	// Time Adjustments
	$year_range = 1;
	$since_date = date('Y-01-01', strtotime('-' . $year_range . ' years'));
	$until_date = date('Y-01-01', strtotime('+' . $year_range . ' years'));
	$since = strtotime($since_date);
	$until = strtotime($until_date);
	// or you can set a fix date range:
	// $since = strtotime("2012-01-08");
	// $until = strtotime("2018-06-28");


	date_default_timezone_set('America/New_York');

	$fields="id,name,description,place,timezone,start_time,cover";



	
	$json_link = "https://graph.facebook.com/v2.7/{$pageid}/events/attending/?fields={$fields}&access_token={$access_token}&since={$since}&until={$until}";
	$json = file_get_contents($json_link);
	$obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
	$numevents =count($obj['data']);
	$today = date('Ymd');
	$upcoming_events=[];
	$past_events=[];
	// Filter Events Raw Data
	for ($x=0; $x < $numevents; $x++) { 
		$start_date = date( 'l, F d, Y', strtotime($obj['data'][$x]['start_time']));
		$start_time = date('g:i a', strtotime($obj['data'][$x]['start_time']));
		$raw_time = strtotime($obj['data'][$x]['start_time']);
		$eid = $obj['data'][$x]['id'];
		$name = $obj['data'][$x]['name'];
		$pic_big = isset($obj['data'][$x]['cover']['source']) ? $obj['data'][$x]['cover']['source'] : "https://graph.facebook.com/v2.7/{$fb_page_id}/picture?type=large";
		$description = isset($obj['data'][$x]['description']) ? $obj['data'][$x]['description'] : "";
		$place_name = isset($obj['data'][$x]['place']['name']) ? $obj['data'][$x]['place']['name'] : "";
		$city = isset($obj['data'][$x]['place']['location']['city']) ? $obj['data'][$x]['place']['location']['city'] : "";
		$country = isset($obj['data'][$x]['place']['location']['country']) ? $obj['data'][$x]['place']['location']['country'] : "";
		$zip = isset($obj['data'][$x]['place']['location']['zip']) ? $obj['data'][$x]['place']['location']['zip'] : "";
		$state = isset($obj['data'][$x]['place']['location']['zip']) ? $obj['data'][$x]['place']['location']['state'] : "";
		$location="";
		if($place_name && $city && $country && $zip){
		    $location="{$place_name}, {$city}, {$country}, {$zip}";
		} 
		else {
			// Location not set or event data is too old.
		    $location = null;
		}

		$place_id = isset($obj['data'][$x]['place']['id']) ? $obj['data'][$x]['place']['id'] : '';

		$temp_array = [
			'start_date' => $start_date,
			'start_time' => $start_time,
			'eid' => $eid,
			'name' => $name,
			'cover' => $pic_big,
			'description' => $description,
			'place_name' => $place_name,
			'city' => $city,
			'country' => $country,
			'zip' => $zip,
			'location' => $location,
			'place_id' => $place_id,
			'raw_time' => $raw_time,
			'state' => $state,
		];
		// Filter Upcoming / Past
		$event_start_formatted = date('Ymd', strtotime($obj['data'][$x]['start_time']));
		if ( $today <= $event_start_formatted){
			array_push($upcoming_events, $temp_array);
		} else {
			array_push($past_events, $temp_array);
		}
	}
 ?>