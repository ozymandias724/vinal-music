<?php 
	$bios = get_field('band_members', 'options');
	if ( !empty($bios) ) :
		foreach ($bios as $bio) :
	 ?>
			<div class="about-members-member revealer">
				
				<div class="about-members-member-info revealer">
					<h3><?php echo $bio['name']; ?></h3>
					<p><?php echo $bio['instrument']; ?></p>
				</div>

				<div class="about-members-member-bg"  style="background-image: url(<?php echo $bio['image']; ?>)"></div>
			</div>
	<?php
		endforeach;
	endif;
 ?>
<div class="desktop-bandbios">
	<div class="desktop-bandbios-members">
		<div class="desktop-bandbios-members--centerer">
			<?php $i = 0; foreach( $bios as $bio ) : $i++ ?>
				<div class="desktop-bandbios-members-member revealer <?php if($i == 1) { echo 'selected-bandmember chosen-bandmember';} ?>">
					<h1><?php echo $bio['name']; ?></h1>
					<p><?php echo $bio['instrument']; ?></p>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	
	<div class="desktop-bandbios-imgframe">
		<?php $i = 0; foreach( $bios as $bio ) : $i++ ?>
			<div class="parallaxbg desktop-bandbios-imgframe-image <?php if($i == 1) { echo 'selected-bandmember-image';} ?>" style="background-image: url(<?php echo $bio['image']; ?>);"></div>
		<?php endforeach; ?>
	</div>
</div>
