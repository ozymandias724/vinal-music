
<div class="hero-content">

	<div class="hero-content-title revealer">
			<span>Grace</span>
			<span>and</span>
			<span>Bile</span>
	</div>
	<p class="hero-content-leadin revealer">The new E.P. by VINAL</p>
	<div class="hero-content-buynow button revealer">
		<a class="button-text" href="https://vinalband.bandcamp.com/" target="_blank">Buy it Now</a>
	</div>
</div>

<video class="hero-bgvid revealer" autoplay="true" loop="true" src="<?php echo get_template_directory_uri(); ?>/library/vid/VinalShowClip.mp4"></video>