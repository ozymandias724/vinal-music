<?php 
	// FaceBook Events JSON module
	include locate_template( 'modules/get_facebook_events.php');
	if(!empty($upcoming_events)) :
		echo '<div class="events-upcoming events--wrapper">';
		foreach ($upcoming_events as $upcoming_event) :
			$cleanmonth = date('M',$upcoming_event['raw_time']);
			$cleandate = date('j',$upcoming_event['raw_time']);
			$cleantime_min = date('\:i',$upcoming_event['raw_time']);
			$cleantime_ampm = date('a',$upcoming_event['raw_time']);
			$cleantime_hour = date('g',$upcoming_event['raw_time']);
			// case-by-case venue name cleanup
			if($upcoming_event['place_name'] == "John Harvard's Brewery & Ale House | Cambridge"){
				$upcoming_event['place_name'] = "John Harvard's";
			}
 ?>
			<div class="events-event revealer">

				<!-- day of the month big in background -->
				<div class="events-event-cleandate">
					
					<p class="events-event-cleandate-date"><?php echo $cleandate; ?></p>
					
					<p class="events-event-cleandate-month"><?php echo $cleanmonth; ?></p>


					<div class="events-event-cleantime">
						<p class="events-event-cleantime-hour"><?php echo $cleantime_hour; ?></p>
						<p class="events-event-cleantime-min"><?php echo $cleantime_min; ?></p>
						<p class="events-event-cleantime-ampm"><?php echo $cleantime_ampm; ?></p>
					</div>
					
				</div>
				<!-- centerer -->

				<div class="events-event-venue">
					<p class="events-event-venue-name"><?php echo $upcoming_event['place_name']; ?>, </p>
					<p class="events-event-venue-location"><?php echo $upcoming_event['city']; echo ' ' . $upcoming_event['state']; ?></p>
				</div>

				<!-- view on facebook  -->
				<div class="events-event-fblink">
					<a class="events-event-fblink-link" href='https://www.facebook.com/events/<?php echo $upcoming_event['eid']; ?>/' target='_blank'>View on Facebook</a>
				</div>
				<!-- background image -->
				<div class="events-past-event-bg events-event-bg" style="background-image: url('<?php echo $upcoming_event['cover']; ?>')"></div>
 			</div>

<?php 	
		endforeach; 
		echo '</div>';
		echo '<div class="events-showrecent"><p class="button"><span class="button-text">Show Recent Events</span></p></div>';
	else : 
		// Fallback for when No Events
 ?>
 		<div class="events-noupcoming revealer">
			<h1 class="events-noupcoming-title">Sorry, no upcoming events...</h1>
			<a class="button" href="#"><p class="button-text">Check Our FaceBook</p></a>					

			<?php 
				if( !empty($past_events)) :
			 ?>
			 	<p class="events-noupcoming-or">or...</p>
			 	<br>
			<?php 
				endif;
			 ?>
			 <!-- Backgrounds -->
 			<div class="events-noupcoming-bg parallaxbg" style="background-image: url('<?php echo get_template_directory_uri() . '/library/img/eventbg.jpg' ?>');"></div>
 			<div class="events-noupcoming-bg mobilebg" style="background-image: url('<?php echo get_template_directory_uri() . '/library/img/eventbg.jpg' ?>');"></div>
 		</div>
 		
<?php 
	endif;
	// Display Past Events (Optional, Toggle inside No Upcoming)
	if( !empty($past_events)) :
		echo '<div class="events-past events--wrapper">';
		// Display only X Past Events (3)
		foreach ($past_events as $index => $past_event) :
			$cleanmonth = date('M',$past_event['raw_time']);
			$cleandate = date('j',$past_event['raw_time']);
			$cleantime_min = date('\:i',$past_event['raw_time']);
			$cleantime_ampm = date('a',$past_event['raw_time']);
			$cleantime_hour = date('g',$past_event['raw_time']);
			// case-by-case venue name cleanup
			if($past_event['place_name'] == "John Harvard's Brewery & Ale House | Cambridge"){
				$past_event['place_name'] = "John Harvard's";
			}
			if($index < 3) :
				$is_odd = ($index % 2 == 0) ? 'events-event--odd' : '';
 ?>				
				<div class="events-event revealer">

				<!-- day of the month big in background -->
				<div class="events-event-cleandate">
					<p class="events-event-cleandate-date"><?php echo $cleandate; ?></p>
					<p class="events-event-cleandate-month"><?php echo $cleanmonth; ?>
						<div class="events-event-cleantime">
							<p class="events-event-cleantime-hour"><?php echo $cleantime_hour; ?></p>
							<p class="events-event-cleantime-min"><?php echo $cleantime_min; ?></p>
							<p class="events-event-cleantime-ampm"><?php echo $cleantime_ampm; ?></p>
						</div>
					</p>
					
				</div>
				<!-- centerer -->

				<div class="events-event-venue">
					<p class="events-event-venue-name"><?php echo $past_event['place_name']; ?>, </p>
					<p class="events-event-venue-location"><?php echo $past_event['city']; echo ' ' . $past_event['state']; ?></p>
				</div>

				<!-- view on facebook  -->
				<div class="events-event-fblink">
					<a class="events-event-fblink-link" href='https://www.facebook.com/events/<?php echo $past_event['eid']; ?>/' target='_blank'>View on Facebook</a>
				</div>
				<!-- background image -->
				<div class="events-past-event-bg events-event-bg" style="background-image: url('<?php echo $past_event['cover']; ?>')"></div>
 			</div>
<?php 
			endif;
		endforeach;
		echo '</div>';
	endif;
 ?>