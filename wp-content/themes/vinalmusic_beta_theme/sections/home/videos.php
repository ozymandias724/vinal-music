<div class="homepage-videos-vidwrap revealer">

	<!-- fetch oembeds -->
	<?php 
		if ( have_rows( 'featuredvideos', 'option' ) ) : while ( have_rows( 'featuredvideos', 'option' ) ) : the_row(); ?>
			<?php the_sub_field( 'youtube' ); ?>
			<?php endwhile; ?>
		<?php endif;
	 ?>
</div>


<div class="parallaxbg"></div>
<div class="mobilebg"></div>