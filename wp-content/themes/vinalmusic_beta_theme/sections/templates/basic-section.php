<?php 
	// Scaffolding Template
 ?>
<section class="section <?php echo $name; ?>">

	<div class="section-banner <?php echo $name . '-banner'; ?>">
		<h1 class="section-banner-title revealer <?php echo $name . '-banner-title'; ?>"><?php echo $name; ?></h1>
	</div>
	
	<?php include( locate_template( 'sections/home/' . $name . '.php' ) ); ?>

</section>