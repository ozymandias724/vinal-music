<footer class="footer">
	<p>&#169; Copyright <?php echo date(Y); ?> VINAL-music</p>
	<p><a href="">Site Map</a></p>
	<p><a href="">Privacy Policy</a></p>
	<p>created by<a href="http://kylemarcy.com"> &#64;KDM</p></a>
</footer>
<?php
	if (in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1', '::1'))) {
	   ?> <script src="//localhost:35729/livereload.js"></script> <?php
	}

	wp_footer();
?>
	</body>
</html>